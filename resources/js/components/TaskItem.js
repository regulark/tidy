import React from 'react'
import PropTypes from 'prop-types'
import TaskDelete from './TaskDelete'
import TaskValue from './TaskValue'
import TaskCompleted from './TaskCompleted'
import ListItem from '@material-ui/core/ListItem'

const TaskItem = ({ task, onChange, onToggle, onDelete }) => (
  <ListItem>
    <TaskCompleted
      task={task}
      onToggle={onToggle}
    />
    <TaskValue
      task={task}
      onChange={onChange}
    />
    <TaskDelete
      task={task}
      onDelete={onDelete}
    />
  </ListItem>
)

TaskItem.propTypes = {
  task: PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
}

export default TaskItem