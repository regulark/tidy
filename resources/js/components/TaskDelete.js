import React from 'react'
import PropTypes from 'prop-types'
import DeleteIcon from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'

const TaskDelete = ({ onDelete }) => (
  <IconButton
    edge="end"
    aria-label="delete"
    onClick={() => onDelete()}
  >
    <DeleteIcon />
  </IconButton>
)

TaskDelete.propTypes = {
  task: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
}

export default TaskDelete