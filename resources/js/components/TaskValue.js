import React from 'react'
import PropTypes from 'prop-types'
import DebounceTextArea from './DebounceTextArea'

const TaskValue = ({ task, onChange }) => (
  <DebounceTextArea
    placeholder="Task"
    maxLength="100"
    value={task.value || ''}
    preChange={(event) => event.target.value.replace('\n', '')}
    onChange={(title) => onChange(title)}
  />
)

TaskValue.propTypes = {
  task: PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
}

export default TaskValue 