import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Skeleton from '@material-ui/lab/Skeleton'
import DebounceTextArea from './DebounceTextArea'

const WhiteboardDescription = ({whiteboard, updateWhiteboard}) => {
    const handleChange = (description) => {
        updateWhiteboard({ description })
    }

    return (
        <Box display="block" pb={2} px={1} m={1}>
            { whiteboard.loaded ? (
                <DebounceTextArea
                    inputProps={{
                        maxLength: 500,
                    }}
                    label="Description"
                    maxLength="500"
                    rowsMax="6"
                    value={whiteboard.description || ''}
                    onChange={description => handleChange(description)}
                />
            ) : (
                <Skeleton variant="rect" height={32} />
            )}
        </Box>
    )
}

WhiteboardDescription.propTypes = {
    whiteboard: PropTypes.object.isRequired,
    updateWhiteboard: PropTypes.func.isRequired,
}

export default WhiteboardDescription
