import React from 'react'
import AboutDialog from './AboutDialog'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import HelpIcon from '@material-ui/icons/Help'
import CreateIcon from '@material-ui/icons/Create'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
    },
}));

const AppHeader = () => {
    const classes = useStyles()
    const [aboutOpen, setAboutOpen] = React.useState(false)

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography
                        variant="h6"
                        className={classes.title}
                    >
                        Tidy
                    </Typography>
                    <IconButton
                        title="Create new whiteboard"
                        aria-label="Create new whiteboard"
                        aria-haspopup="true"
                        color="inherit"
                        href="/new"
                    >
                        <CreateIcon />
                    </IconButton>
                    <IconButton
                        label="About Tidy"
                        aria-label="About Tidy"
                        aria-haspopup="true"
                        color="inherit"
                        onClick={() => setAboutOpen(true)}
                    >
                        <HelpIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <AboutDialog
                open={aboutOpen}
                onClose={() => setAboutOpen(false)}
            />
        </div>
    )
}

export default AppHeader