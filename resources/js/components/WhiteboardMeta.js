import React from 'react'
import Moment from 'moment'
import PropTypes from 'prop-types'
import Box from '@material-ui/core/Box'
import Skeleton from '@material-ui/lab/Skeleton'
import Typography from '@material-ui/core/Typography'

const WhiteboardMeta = ({whiteboard}) => (
    <Box display="block" pb={2} px={1} m={1} color="text.secondary">
        <Typography variant="body2">
            { whiteboard.loaded ? (
                <span>Created { Moment(whiteboard.created_at).fromNow() }.</span>
            ) : (
                <Skeleton component="span"></Skeleton>
            )}
        </Typography>
    </Box>
)

WhiteboardMeta.propTypes = {
    whiteboard: PropTypes.object.isRequired,
}

export default WhiteboardMeta