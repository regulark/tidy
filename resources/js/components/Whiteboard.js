import React from 'react'
import PropTypes from 'prop-types'
import TaskList from './TaskList'
import TaskCreate from './TaskCreate'
import Box from '@material-ui/core/Box'
import Paper from '@material-ui/core/Paper'
import WhiteboardMeta from './WhiteboardMeta'
import WhiteboardTitle from './WhiteboardTitle'
import VisibilityFilters from './VisibilityFilters'
import WhiteboardDescription from './WhiteboardDescription'

const Whiteboard = ({
    whiteboard,
    updateWhiteboard,
    tasks,
    createTask,
    updateTask,
    deleteTask,
}) => (
    <React.Fragment>
        <Box mb={3}>
            <Paper>
                <WhiteboardTitle
                    whiteboard={whiteboard}
                    updateWhiteboard={updateWhiteboard}
                />
                <WhiteboardDescription
                    whiteboard={whiteboard}
                    updateWhiteboard={updateWhiteboard}
                />
                <WhiteboardMeta
                    whiteboard={whiteboard}
                />
            </Paper>
        </Box>
        <Box mb={3}>
            <Paper>
                <VisibilityFilters />
                <TaskList
                    tasks={tasks}
                    updateTask={updateTask}
                    deleteTask={deleteTask}
                />
            </Paper>
        </Box>
        <Paper>
            <TaskCreate
                disabled={!tasks.loaded}
                createTask={createTask}
            />
        </Paper>
    </React.Fragment>
)

Whiteboard.propTypes = {
    whiteboard: PropTypes.object.isRequired,
    updateWhiteboard: PropTypes.func.isRequired,
    tasks: PropTypes.object.isRequired,
    createTask: PropTypes.func.isRequired,
    updateTask: PropTypes.func.isRequired,
    deleteTask: PropTypes.func.isRequired,
}

export default Whiteboard;