import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

const AboutDialog = ({ onClose, open }) => {
    const handleClose = () => {
        onClose()
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="about-dialog-title"
            aria-describedby="about-dialog-description"
        >
            <DialogTitle id="about-dialog-title">About Tidy</DialogTitle>
            <DialogContent>
                <DialogContentText id="about-dialog-description">
                    {"With Tidy, getting your friend and family organized is easy. Just pop onto Tidy, start typing what needs to be done, and share the link with your friends! Done."}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose} color="primary" autoFocus>
                Okay
            </Button>
            </DialogActions>
        </Dialog>
    );
}

AboutDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
}

export default AboutDialog