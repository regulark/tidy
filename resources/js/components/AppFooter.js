import React from 'react'
import Box from '@material-ui/core/Box'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'

const AppFooter = () => (
    <Typography component="div">
        <Box textAlign="center" m={3} color="text.secondary">
            made by <Link href="https://seankarol.com">sean karol</Link> with love
        </Box>
    </Typography>
)

export default AppFooter