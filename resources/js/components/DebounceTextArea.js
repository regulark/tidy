import React from 'react'
import PropTypes from 'prop-types'
import debounce from 'lodash/debounce'
import TextField from '@material-ui/core/TextField';

class DebounceTextArea extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            value: this.props.value || ''
        }

        this.changed = debounce(this.props.onChange, 250)
        this.ref = React.createRef()
    }

    handleChange(event) {
        const value = this.props.preChange ?
                      this.props.preChange(event) :
                      event.target.value

        this.setState({ value }, () => {
            this.changed(value)
        })
    }

    componentDidUpdate(prevProps) {
        // If the prop value has changed, then update the state value to match.
        if ( this.props.value !== prevProps.value ) {
            this.setState({ value: this.props.value })
        }
    }

    render() {
        // eslint-disable-next-line no-unused-vars
        const { preChange, onChange, ...props } = this.props

        return (
            <TextField
                {...props}
                type="text"
                value={this.state.value}
                ref={this.ref}
                fullWidth
                multiline
                onChange={() => null}
                onInput={() => this.handleChange(event)}
            />
        );
    }
}

DebounceTextArea.propTypes = {
    value: PropTypes.string.isRequired,
    preChange: PropTypes.func,
    onChange: PropTypes.func.isRequired,
}

export default DebounceTextArea