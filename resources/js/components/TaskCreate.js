import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const useStyles = makeStyles(() => ({
    container: {
        display: 'flex',
        alignItems: 'center',
    },
    textField: {},
    submitButton: {}
}))

const TaskCreate = ({ disabled, createTask }) => {
    const classes = useStyles()
    const [value, setValue] = React.useState('')

    const trimReturns = (value) => {
        return value.replace('\n', '')
    }

    const handleChange = (event) => {
        setValue(trimReturns(event.target.value))
    }

    const handleKeyDown = (event) => {
        if ( 13 === event.keyCode ) {
            handleSubmit(event)
        }
    }

    const handleSubmit = (event) => {
        if ( !disabled && value ) {
            createTask({ value })
            setValue('')
        }
        
        event.preventDefault()
    }

    return (
        <form onSubmit={(event) => handleSubmit(event)}>
            <Box display="block" p={1} m={1} className={classes.container}>
                <TextField
                    disabled={disabled}
                    inputProps={{
                        maxLength: 100,
                    }}
                    placeholder="Add a new task"
                    className={classes.textField}
                    multiline
                    fullWidth
                    autoFocus
                    value={value || ''}
                    onKeyDown={(event) => handleKeyDown(event)}
                    onChange={(event) => handleChange(event)}
                />
                <IconButton
                    type="submit"
                    disabled={disabled || !value}
                    aria-label="submit"
                    className={classes.submitButton}
                    color="secondary"
                    edge="end"
                >
                    <AddCircleIcon />
                </IconButton>
            </Box>
        </form>
    )
}

TaskCreate.propTypes = {
    disabled: PropTypes.bool.isRequired,
    createTask: PropTypes.func.isRequired,
}

export default TaskCreate