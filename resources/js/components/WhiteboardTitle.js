import React from 'react'
import PropTypes from 'prop-types'
import Box from '@material-ui/core/Box'
import Skeleton from '@material-ui/lab/Skeleton'
import DebounceTextArea from './DebounceTextArea'

const WhiteboardTitle = ({ whiteboard, updateWhiteboard }) => {
    const preChange = (event) => {
        return event.target.value.replace('\n', '')
    }

    const handleChange = (title) => {
        updateWhiteboard({ title })
    }

    const catchReturn = (event) => {
        if ( 13 === event.keyCode ) {
            event.preventDefault()
        }
    }

    return (
        <Box display="block" py={2} px={1} m={1}>
            {whiteboard.loaded ? (
                <DebounceTextArea
                    inputProps={{
                        maxLength: 100,
                    }}
                    label="Title"
                    value={whiteboard.title || ''}
                    onKeyDown={(event) => catchReturn(event)}
                    preChange={event => preChange(event)}
                    onChange={title => handleChange(title)}
                />
            ) : (
                <Skeleton variant="rect" height={32} />
            )}
        </Box>
    )
}

WhiteboardTitle.propTypes = {
    whiteboard: PropTypes.object.isRequired,
    updateWhiteboard: PropTypes.func.isRequired,
}

export default WhiteboardTitle;
