import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { VISIBILITY_FILTERS } from '../constants';
import { setTasksVisibilityFilter } from '../redux/actions/tasks';
import { getTasksByVisibilityFilter } from '../redux/selectors';

const VisibilityFilters = ({ tasks, activeFilter, setTasksVisibilityFilter }) => {
    const handleChange = (event, newValue) => {
        setTasksVisibilityFilter(VISIBILITY_FILTERS[newValue])
    }

    return (
        <Tabs
            value={activeFilter}
            onChange={handleChange}
            aria-label="Set task visibility"
            variant="fullWidth"
        >
            {Object.keys(VISIBILITY_FILTERS).map((filterKey) => {
                const currentFilter = VISIBILITY_FILTERS[filterKey]
                const amount = getTasksByVisibilityFilter(tasks, currentFilter).length
                return (
                    <Tab label={`${currentFilter} (${amount})`} value={filterKey} key={currentFilter} />
                )
            })}
        </Tabs>
    )
}

VisibilityFilters.propTypes = {
    tasks: PropTypes.array.isRequired,
    activeFilter: PropTypes.string.isRequired,
    setTasksVisibilityFilter: PropTypes.func.isRequired,
}
  
const mapStateToProps = state => ({
    tasks: state.tasks.items,
    activeFilter: state.tasks.visibilityFilter 
})

export default connect(
    mapStateToProps,
    { setTasksVisibilityFilter }
)(VisibilityFilters);