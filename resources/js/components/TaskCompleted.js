import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from '@material-ui/core/Checkbox'

const TaskCompleted = ({ task, onToggle }) => (
  <Checkbox
    edge="start"
    type="checkbox"
    checked={task.completed}
    onChange={(event) => onToggle(event.target.checked)}
  />
)

TaskCompleted.propTypes = {
  task: PropTypes.object.isRequired,
  onToggle: PropTypes.func.isRequired,
}

export default TaskCompleted