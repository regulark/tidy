import React from 'react';
import TaskItem from './TaskItem';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import Skeleton from '@material-ui/lab/Skeleton'
import Typography from '@material-ui/core/Typography';
import { getTasksByVisibilityFilter } from '../redux/selectors';

const TaskList = ({ tasks, updateTask, deleteTask }) => {
  const listItems = (tasks) => (
    tasks.map(task => (
      <TaskItem
        key={task.id}
        task={task}
        {...task}
        onToggle={(completed) => updateTask(task.id, { completed })}
        onChange={(value) => updateTask(task.id, { value })}
        onDelete={() => deleteTask(task.id)}
      />
    ))
  )

  const emptyItems = (items,) => {
    let text = `There are no tasks here.`
    if ( items.length === 0 ) {
      text = `Start by adding tasks below.`
    }

    return (
      <Typography component="div">
        <Box color="text.secondary" py={2.5} mx={2}>
          <span>{ text }</span>
        </Box>
      </Typography>
    )
  }

  const skeletonItems = () => (
    <Box color="text.secondary" py={2} mx={2}>
      <Skeleton variant="rect" height={36} />
    </Box>
  )

  const visibleItems = getTasksByVisibilityFilter(tasks.items, tasks.visibilityFilter)

  return (
    <List>
      {tasks.loaded ? (
        visibleItems.length ? listItems(visibleItems) : emptyItems(tasks.items)
      ) : (
        skeletonItems()
      )}
    </List>
  )
}

TaskList.propTypes = {
  tasks: PropTypes.object.isRequired,
  updateTask: PropTypes.func.isRequired,
  deleteTask: PropTypes.func.isRequired,
}

export default TaskList