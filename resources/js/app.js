/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import React from 'react'
import thunk from 'redux-thunk'
import ReactDOM from 'react-dom'
import App from './containers/App'
import { Provider } from 'react-redux'
import rootReducer from './redux/reducers/index'
import { createStore, applyMiddleware } from 'redux'

const rootElement = document.getElementById('app')
const store = createStore(rootReducer, applyMiddleware(thunk))

ReactDOM.render(
    <Provider store={store}>
        <App data={Object.assign({}, rootElement.dataset)} />
    </Provider>,
    rootElement,
)