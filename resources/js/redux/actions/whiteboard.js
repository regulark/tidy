import axios from 'axios';
import {
    UPDATE_WHITEBOARD,
} from './actionTypes';

export const endpoint = (slug) => (`/api/v2/whiteboards/${slug}`)

export const fetchWhiteboard = (slug) => {
    return (dispatch) => {
        return axios.post(endpoint(slug))
            .then(({ data }) => {
                dispatch(updateWhiteboard(data.data))
            })
            .catch((error) => {
                dispatch(updateWhiteboard({ failed: true }))
                throw error
            })
    }
}

export const updateWhiteboard = (changes) => ({
    type: UPDATE_WHITEBOARD,
    data: { ...changes },
})

export const userUpdateWhiteboard = (slug, changes) => {
    const data = {
        ...changes,
        _method: 'patch',
    }

    axios.post(endpoint(slug), data)
    .catch((err) => {
        throw err;
    });
    
    return updateWhiteboard(changes);
}