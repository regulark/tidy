import axios from 'axios';
import {
    UPDATE_TASKS,
    UPDATE_TASK,
    CREATE_TASK,
    DELETE_TASK,
    SET_TASKS_VISIBILITY,
} from './actionTypes';

export const endpoint = (slug) => (`/api/v2/whiteboards/${slug}/tasks`)

export const fetchTasks = (slug) => {
    return (dispatch) => {
        return axios.get(endpoint(slug))
        .then(({ data }) => {
            dispatch(updateTasks(data.data))
        })
    }
}

export const createTask = (attributes) => ({
    type: CREATE_TASK,
    data: attributes,
})

export const userCreateTask = (slug, attributes) => {
    return (dispatch) => {
        const data = {
            ...attributes,
            _method: 'POST',
        }

        return axios.post(endpoint(slug), data)
            .then(({ data }) => {
                dispatch(createTask(data.data))
            })
    }
}

export const updateTasks = (data) => ({
    type: UPDATE_TASKS,
    data,
})

export const updateTask = (id, changes) => ({
    type: UPDATE_TASK,
    data: { id, ...changes },
})

export const userUpdateTask = (slug, id, changes) => {
    const data = {
        id,
        ...changes,
        _method: 'PATCH',
    }
    
    axios.post(endpoint(slug), data)
    .catch((err) => {
        throw err;
    });

    return updateTask(id, data);
}


export const userDeleteTask = (slug, id) => {
    const data = {
        id,
        _method: 'DELETE',
    }
    
    axios.post(endpoint(slug), data)
    .catch((err) => {
        throw err;
    });

    return deleteTask(id)
}

export const deleteTask = (id) => ({
    type: DELETE_TASK,
    data: { id },
})

export const setTasksVisibilityFilter = (filter) => ({
    type: SET_TASKS_VISIBILITY,
    data: { filter },
})