import { VISIBILITY_FILTERS } from '../constants'

export const getTasks = store => store.tasks.items

export const getTaskById = (tasks, id) =>
    tasks.find((task) => task.id === id)

export const getTasksByVisibilityFilter = (tasks, visibilityFilter) => {
    switch (visibilityFilter) {
        case VISIBILITY_FILTERS.COMPLETED:
            return tasks.filter(task => task.completed)
        case VISIBILITY_FILTERS.INCOMPLETE:
            return tasks.filter(task => !task.completed)
        case VISIBILITY_FILTERS.ALL:
        default:
            return tasks
    }
}