import { UPDATE_WHITEBOARD } from '../actions/actionTypes';

const initialState = {
    loaded: false,
    failed: false,
}

const whiteboard = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_WHITEBOARD: {
            const { data } = action;

            return {
                ...state,
                ...data,
                loaded: true,
            };
        }
        default:
            return state;
    }
}

export default whiteboard