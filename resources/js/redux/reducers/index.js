import { combineReducers } from 'redux';
import whiteboard from './whiteboard';
import tasks from './tasks';

export default combineReducers({ whiteboard, tasks });
