import { VISIBILITY_FILTERS } from '../../constants';
import {
    CREATE_TASK,
    UPDATE_TASKS,
    UPDATE_TASK,
    DELETE_TASK,
    SET_TASKS_VISIBILITY,
} from '../actions/actionTypes'
import { getTaskById } from '../selectors'

const initialState = {
    loaded: false,
    visibilityFilter: VISIBILITY_FILTERS.INCOMPLETE,
    items: [],
}

const tasks = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_TASK: {
            if ( getTaskById(state.items, action.data.id ) ) {
                return state
            }
            
            return {
                ...state,
                items: state.items.concat([
                    action.data,
                ])
            }
        }
        case UPDATE_TASKS: {
            return {
                ...state,
                loaded: true,
                items: action.data,
            }
        }
        case UPDATE_TASK: {
            const { id, ...changes } = action.data

            return {
                ...state,
                items: state.items.map((task) => {
                    return task.id === id ? { ...task, ...changes } : { ...task }
                })
            }
        }
        case DELETE_TASK: {
            const id = action.data.id

            return {
                ...state,
                items: state.items.filter((task) => {
                    return task.id !== id
                }) 
            }
        }
        case SET_TASKS_VISIBILITY: {
            return {
                ...state,
                visibilityFilter: action.data.filter
            }
        }
        default:
            return state;
    }
}

export default tasks