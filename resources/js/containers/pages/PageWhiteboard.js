/* globals Echo */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Whiteboard from '../../components/Whiteboard';
import {
    fetchWhiteboard,
    updateWhiteboard,
    userUpdateWhiteboard
} from '../../redux/actions/whiteboard';
import {
    fetchTasks,
    createTask,
    updateTask,
    deleteTask,
    userCreateTask,
    userUpdateTask,
    userDeleteTask,
} from '../../redux/actions/tasks';

class PageWhiteboard extends React.Component {
    componentDidMount() {        
        const slug = this.props.slug

        this.props.fetchWhiteboard(slug)
        this.props.fetchTasks(slug)

        Echo.channel(`whiteboard.${slug}`)
            .listen('WhiteboardUpdated', (e) => { this.props.updateWhiteboard(e.data) })
            .listen('TaskCreated', (e) => { this.props.createTask(e.task) })
            .listen('TaskUpdated', (e) => { this.props.updateTask(e.data.id, e.data) })
            .listen('TaskDeleted', (e) => { this.props.deleteTask(e.task.id) })
    }

    render() {
        return (
            <React.Fragment>
                <Whiteboard
                    whiteboard={this.props.whiteboard}
                    tasks={this.props.tasks}
                    updateWhiteboard={this.props.userUpdateWhiteboard}
                    createTask={this.props.userCreateTask}
                    updateTask={this.props.userUpdateTask}
                    deleteTask={this.props.userDeleteTask}
                />
            </React.Fragment>
        )
    }
}

PageWhiteboard.propTypes = {
    slug: PropTypes.string.isRequired,
    whiteboard: PropTypes.object.isRequired,
    tasks: PropTypes.object.isRequired,
    fetchWhiteboard: PropTypes.func.isRequired,
    updateWhiteboard: PropTypes.func.isRequired,
    fetchTasks: PropTypes.func.isRequired,
    createTask: PropTypes.func.isRequired,
    updateTask: PropTypes.func.isRequired,
    deleteTask: PropTypes.func.isRequired,
    userUpdateWhiteboard: PropTypes.func.isRequired,
    userCreateTask: PropTypes.func.isRequired,
    userUpdateTask: PropTypes.func.isRequired,
    userDeleteTask: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => {
    return {
        whiteboard: state.whiteboard,
        tasks: state.tasks,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    fetchWhiteboard: (slug) => dispatch(fetchWhiteboard(slug)),
    updateWhiteboard: (changes) => dispatch(updateWhiteboard(changes)),
    fetchTasks: (slug) => dispatch(fetchTasks(slug)),
    createTask: (attributes) => dispatch(createTask(attributes)),
    updateTask: (id, changes) => dispatch(updateTask(id, changes)),
    deleteTask: (id) => dispatch(deleteTask(id)),
    userUpdateWhiteboard: (changes) => dispatch(userUpdateWhiteboard(ownProps.slug, changes)),
    userCreateTask: (attributes) => dispatch(userCreateTask(ownProps.slug, attributes)),
    userUpdateTask: (id, changes) => dispatch(userUpdateTask(ownProps.slug, id, changes)),
    userDeleteTask: (id) => dispatch(userDeleteTask(ownProps.slug, id)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PageWhiteboard)