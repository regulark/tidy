import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'

const PageError = ({ statusCode }) => {
    const codes = {
        '404': {
            'title': 'Page Not Found',
            'body': 'If you were looking for a whiteboard, it probably expired.',
        },
    }

    const code = typeof codes[parseInt(statusCode)] === 'object' ? codes[parseInt(statusCode)] : {
        'title': 'Uknown Error',
        'body': 'Something unexpected has happened!',
    }

    return (
        <Typography align="center" component="div">
            <Typography component="div">
                <h2>{code['title']}</h2>
                <p>{code['body']}</p>
                <Box color="text.secondary">Code {statusCode}</Box>
            </Typography>
        </Typography>
    )
}

PageError.propTypes = {
    statusCode: PropTypes.string.isRequired,
}

export default PageError