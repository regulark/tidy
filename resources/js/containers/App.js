import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import PageError from './pages/PageError';
import AppHeader from '../components/AppHeader';
import AppFooter from '../components/AppFooter';
import PageWhiteboard from './pages/PageWhiteboard';
import Container from '@material-ui/core/Container';
import purple from '@material-ui/core/colors/purple';
import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const App = ({ data }) => {
    const outerTheme = createMuiTheme({
        palette: {
            primary: purple,
            secondary: {
                main: '#f44336',
            },
        },
    })

    const slug = window.location.pathname.match(/([\w-]+)/)[0]

    return (
        <ThemeProvider theme={outerTheme}>
            <CssBaseline />
            <Box mb={3}>
                <AppHeader />
            </Box>
            <Container fixed>
                { typeof data.statusCode === 'string' ? (
                    <PageError statusCode={data.statusCode} />
                ) : (
                    <PageWhiteboard slug={slug} />
                )}
            </Container>
            <AppFooter />
        </ThemeProvider>
    )
}

App.propTypes = {
    data: PropTypes.object,
}

export default App