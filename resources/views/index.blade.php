@extends('layouts.app')

@if ( isset($whiteboard) )
    @section('page.subtitle')
        {{ $whiteboard->pageTitle }}
    @endsection
@endif

@section('content')
    @if ( isset($exception) )
        @php $statusCode = $exception->getStatusCode(); @endphp
    @endif

    <div id="app" {!! isset($statusCode) ? 'data-status-code="' . $statusCode . '"' : ''!!}></div>
@endsection