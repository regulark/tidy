<?php

namespace Tests\Setup;

use App\Whiteboard;
use App\User;
use App\Task;

/**
 * The WhiteboardFactory class.
 * 
 * Helps to construct the world within feature tests.
 */
class WhiteboardFactory
{
    protected $tasksCount = 0;

    protected $taskAttributes = [];

    protected $user = null;

    /**
     * Set the amount of tasks to create for the whiteboard.
     *
     * @param int $count
     * @return $this
     */
    public function withTasks($count)
    {
        $this->tasksCount = $count;

        return $this;
    }

    /**
     * Create a specific Task for this whiteboard
     *
     * @param array $attributes
     * @return $this
     */
    public function withTask($attributes)
    {
        $this->taskAttributes[] = $attributes;

        return $this;
    }

    /**
     * Create the whiteboard.
     *
     * @return void
     */
    public function create()
    {
        $whiteboard = factory(Whiteboard::class)->create();

        factory(Task::class, $this->tasksCount)->create([
            'whiteboard_id' => $whiteboard->id,
        ]);

        foreach ($this->taskAttributes as $task) {
            $task['whiteboard_id'] = $whiteboard->id;
            factory(Task::class)->create($task);
        }

        return $whiteboard;
    }
}