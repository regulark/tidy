<?php

namespace Tests\Feature;

use App\Task;
use App\Whiteboard;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Facades\Tests\Setup\WhiteboardFactory;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_can_store_a_task() {
        $whiteboard = WhiteboardFactory::create();

        $response = $this->post(route('task.store', $whiteboard));

        $response->assertStatus(201, 'Expecting 201 CREATED response.');
        $this->assertEquals(1, $whiteboard->tasks()->count(), 'Expecting 1 task in whiteboard.');
    }

    /** @test */
    public function a_stored_task_returns_the_resource() {
        $whiteboard = WhiteboardFactory::create();

        $response = $this->post(route('task.store', $whiteboard));

        $response->assertJsonStructure([
            'data' => [
                'id',
                'value',
                'completed',
                'order',
            ]
        ]);
    }

    /** @test */
    public function a_guest_can_update_a_task_value() {
        $whiteboard = WhiteboardFactory::withTask(['value' => ''])->create();

        $response = $this->patch(route('task.update', $whiteboard), [
            'id' => $whiteboard->tasks[0]['id'],
            'value' => 'Acme'
        ]);
        $whiteboard->refresh();

        $response->assertStatus(200, 'Expecting 200 OK response.');
        $this->assertEquals('Acme', $whiteboard->tasks[0]['value']);
    }

    /** @test */
    public function an_updated_task_returns_the_resource() {
        $whiteboard = WhiteboardFactory::withTask(['value' => ''])->create();

        $response = $this->patch(route('task.update', $whiteboard), [
            'id' => $whiteboard->tasks[0]['id'],
            'value' => 'Acme'
        ]);
        $whiteboard->refresh();

        $response->assertJsonStructure([
            'data' => [
                'id',
                'value',
                'completed',
                'order',
            ]
        ]);
    }

    /** @test */
    public function an_updated_task_can_be_completed() {
        $whiteboard = WhiteboardFactory::withTask(['value' => 'Acme'])->create();

        $response = $this->patch(route('task.update', $whiteboard), [
            'id' => $whiteboard->tasks[0]['id'],
            'completed' => true,
        ]);
        $whiteboard->refresh();

        $response->assertJsonStructure([
            'data' => [
                'id',
                'value',
                'completed',
                'order',
            ]
        ]);
    }

    /** @test */
    public function an_updated_task_can_be_incompleted() {
        $whiteboard = WhiteboardFactory::withTask(['value' => 'Acme', 'completed' => true])->create();

        $response = $this->patch(route('task.update', $whiteboard), [
            'id' => $whiteboard->tasks[0]['id'],
            'completed' => false,
        ]);
        $whiteboard->refresh();

        $response->assertJsonStructure([
            'data' => [
                'id',
                'value',
                'completed',
                'order',
            ]
        ]);
    }

    /** @test */
    public function an_updated_task_can_be_destroyed() {
        $whiteboard = WhiteboardFactory::withTask(['value' => 'Acme'])->create();

        $response = $this->patch(route('task.update', $whiteboard), [
            'id' => $whiteboard->tasks[0]['id'],
            'value' => ''
        ]);
        $whiteboard->refresh();

        $response->assertSee('destroyed')
                 ->assertOk();
    }

    /** @test */
    public function a_guest_can_delete_a_task() {
        $whiteboard = WhiteboardFactory::withTasks(1)->create();

        $response = $this->delete(route('task.destroy', $whiteboard), [
            'id' => $whiteboard->tasks[0]['id']
        ]);
        
        $response->assertStatus(200, 'Expecting 200 OK response.');
        $this->assertEmpty($whiteboard->tasks, 'Expecting 0 tasks in whiteboard.');
    }

    /** @test */
    public function a_guest_cannot_add_too_many_tasks() {
        $whiteboard = WhiteboardFactory::create();
        factory(Task::class, $whiteboard->tasks_limit)->create(['whiteboard_id' => $whiteboard->id]);

        $response = $this->post(route('task.store', $whiteboard));
        
        $response->assertStatus(422, 'Expecting 422 UNPROCESSABLE ENTITY response.');
        $this->assertEquals($whiteboard->tasks_limit, $whiteboard->tasks()->count(), 'Expecting the whiteboard to meet the task limit.');
    }
}
