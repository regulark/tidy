<?php

namespace Tests\Feature;

use App\Whiteboard;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function first_visit_creates_new_whiteboard()
    {
        $this->assertCount(0, Whiteboard::all());

        $response = $this->get(route('whiteboard.index'));

        $this->assertCount(1, Whiteboard::all());

        $response->assertRedirect(route('whiteboard.edit', ['whiteboard' => Whiteboard::first()]));
    }

    /** @test */
    public function visit_stores_whiteboard_slug()
    {
        $this->withoutExceptionHandling();
        $response = $this->followingRedirects()->get(route('whiteboard.index'));

        $this->assertNotEmpty($response->session()->has('previous_whiteboard'));
    }

    /** @test */
    public function visit_redirects_to_whiteboard_slug()
    {
        $whiteboard = Whiteboard::create();

        $response = $this->withSession(['previous_whiteboard' => $whiteboard->slug])
                         ->followingRedirects()
                         ->get(route('whiteboard.index'));

        $response->assertViewHas('whiteboard', $whiteboard);
    }
}
