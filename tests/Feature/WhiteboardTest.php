<?php

namespace Tests\Feature;

use App\Task;
use App\Whiteboard;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Facades\Tests\Setup\WhiteboardFactory;

class WhiteboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_can_create_a_whiteboard()
    {
        $response = $this->get(route('whiteboard.create'));

        $response->assertRedirect();
        $this->assertEquals(1, Whiteboard::all()->count(), 'Expecting one whiteboard.');
    }

    /** @test */
    public function a_guest_can_edit_a_whiteboard()
    {
        $whiteboard = WhiteboardFactory::create();

        $response = $this->get(route('whiteboard.edit', $whiteboard));

        $response->assertOk();
    }

    /** @test */
    public function a_guest_can_show_a_whiteboard()
    {
        $whiteboard = WhiteboardFactory::create();

        $response = $this->post(route('whiteboard.show', $whiteboard));

        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                'title',
                'description',
                'tasks_limit',
            ],
        ]);
    }

    /** @test */
    public function a_guest_can_update_a_whiteboard()
    {
        $whiteboard = WhiteboardFactory::create();

        $response = $this->patch(route('whiteboard.update', $whiteboard), [
            'title' => 'Acme',
            'description' => 'Lorem ipsum',
        ]);
        $whiteboard->refresh();

        $response->assertOk();
        $this->assertEquals('Acme', $whiteboard->title);
        $this->assertEquals('Lorem ipsum', $whiteboard->description);
    }
}
