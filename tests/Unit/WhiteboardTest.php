<?php

namespace Tests\Unit;

use App\Task;
use Carbon\Carbon;
use App\Whiteboard;
use Tests\TestCase;
use Carbon\CarbonInterval;
use App\Events\WhiteboardUpdated;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\TooManyTasksException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WhiteboardTest extends TestCase
{
    use RefreshDatabase;

    protected $whiteboard;

    public function setUp() : void
    {
        parent::setUp();

        $this->whiteboard = factory(Whiteboard::class, 'empty')->create();
    }

    /** @test */
    public function a_whiteboard_has_a_generated_slug()
    {
        $this->assertNotNull($this->whiteboard->slug, 'The whiteboard does not have a slug.');
    }

    /** @test */
    public function a_whiteboard_has_a_display_title_of_slug_when_title_is_empty()
    {
        $this->whiteboard->title = '';

        $this->assertEquals($this->whiteboard->slug, $this->whiteboard->display_title, 'The whiteboard\'s displayTitle should be it\'s slug if it has no title.');
    }

    /** @test */
    public function a_whiteboard_has_a_display_title_of_title_when_title_is_not_empty()
    {
        $this->whiteboard->title = 'Acme';

        $this->assertNotNull($this->whiteboard->title,
        "The whiteboard should have a title.");

        $this->assertEquals($this->whiteboard->title, $this->whiteboard->display_title, 'The whiteboard\'s displayTitle should be its title if it has a title.');
    }

    /** @test */
    public function a_whiteboard_has_a_channel_name()
    {
        $this->assertNotNull($this->whiteboard->channelName, 'The whiteboard does not have a channel name.');
    }

    /** @test */
    public function a_whiteboard_has_a_tasks_limit()
    {
        $this->assertGreaterThan(0, $this->whiteboard->tasks_limit, 'The whiteboard doesn\'t have a tasks limit.');
    }

    /** @test */
    public function a_whiteboard_can_create_a_task()
    {
        $task = $this->whiteboard->createTask();

        $this->assertNotNull($this->whiteboard->tasks()->find($task), 'The whiteboard could not get a task.');
    }

    /** @test */
    public function a_whiteboard_can_create_a_task_with_default_attributes()
    {
        $task = $this->whiteboard->createTask(['value' => 'Acme', 'completed' => true]);

        $this->assertEquals('Acme', $task->value, 'The tasks value is not Acme.');

        $this->assertEquals(true, $task->completed, 'The task is not marked as completed.');
    }

    /** @test */
    public function a_whiteboard_cannot_have_more_tasks_then_tasks_limit() {
        $this->expectException(TooManyTasksException::class);

        for( $i = 0; $i <= $this->whiteboard->tasks_limit + 1; $i++ ) {
            $tasks = $this->whiteboard->createTask();
        }

        $this->assertCount($this->whiteboard->tasks_limit, $this->whiteboard->tasks, 'The whiteboard\'s task count is not task limit.');
    }

    /** @test */
    public function a_whiteboard_can_update_a_task() {
        $task = $this->whiteboard->createTask();

        $this->whiteboard->updateTask($task, ['value' => 'Acme', 'completed' => true]);

        $task->refresh();

        $this->assertEquals('Acme', $task->value, 'The tasks value wasn\'t updated to Acme.');

        $this->assertEquals(1, $task->completed, 'The task isn\'t marked as completed.');
    }

    /** @test */
    public function a_whiteboard_can_delete_a_task() {
        $task = $this->whiteboard->createTask();

        $this->assertNotNull($this->whiteboard->tasks()->find($task), 'The whiteboard couldn\'t add a task.');

        $this->whiteboard->deleteTask($task);

        $this->assertEmpty($this->whiteboard->tasks()->find($task), 'The whiteboard couldn\'t delete the task.');
    }

    /** @test */
    public function a_whiteboard_broadcasts_a_whiteboard_updated_event()
    {
        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        Model::setEventDispatcher($initialDispatcher);

        $this->whiteboard->update(['title' => 'Acme']);
        $whiteboard = $this->whiteboard;

        Event::assertDispatched(WhiteboardUpdated::class, function ($e) use ($whiteboard) {
            return $e->whiteboard() === $whiteboard && $e->whiteboard()->title === 'Acme';
        }, 'The whiteboard didn\'t dispatch a WhiteboardUpdated event.');
    }

    /** @test */
    public function a_whiteboard_with_tasks_is_not_empty() {
        $this->whiteboard->createTask();

        $this->assertEquals(1, $this->whiteboard->tasks()->count(), 'The whiteboard should have one task.');

        $this->assertFalse($this->whiteboard->isEmpty(), 'The whiteboard has a task but is empty.'
    );
    }

    /** @test */
    public function a_whiteboard_with_a_title_is_not_empty() {
        $this->whiteboard->title = 'Acme';

        $this->assertFalse($this->whiteboard->isEmpty(), 'The whiteboard has a title but is empty.');
    }

    /** @test */
    public function a_whiteboard_with_a_description_is_not_empty() {
        $this->whiteboard->description = 'Acme';

        $this->assertFalse($this->whiteboard->isEmpty(), 'The whiteboard has a description but is empty.');
    }

    /** @test */
    public function a_new_whiteboard_is_empty() {
        $this->assertTrue($this->whiteboard->isEmpty(), 'The whiteboard is unmodified but is not empty.');
    }

    /** @test */
    public function a_new_whiteboard_thats_viewed_is_not_stale() {
        $this->whiteboard->viewed();

        $this->assertFalse($this->whiteboard->stale(), 'The whiteboard is stale.');
    }

    /** @test */
    public function a_new_whiteboard_thats_not_viewed_is_not_stale() {
        $this->assertTrue($this->whiteboard->stale(), 'The whiteboard is stale.');
    }

    /** @test */
    public function a_non_empty_whiteboard_viewed_a_week_ago_is_stale() {
        $this->whiteboard->createTask();

        $this->assertFalse($this->whiteboard->isEmpty(), 'The whiteboard should not be empty with a task.');

        $this->whiteboard->viewed_at = Carbon::now()->sub(CarbonInterval::weeks(1));

        $this->assertTrue($this->whiteboard->stale(), 'The whiteboard should be stale.');
    }

    /** @test */
    public function an_empty_whiteboard_viewed_a_hour_ago_is_stale() {
        $this->whiteboard->viewed_at = Carbon::now()->sub(CarbonInterval::hours(1));

        $this->assertTrue($this->whiteboard->isEmpty(), 'The whiteboard should be empty.');

        $this->assertTrue($this->whiteboard->stale(), 'The whiteboard should be stale.');
    }
}