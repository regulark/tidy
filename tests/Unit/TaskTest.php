<?php

namespace Tests\Unit;

use App\Task;
use App\Whiteboard;
use Tests\TestCase;
use App\Events\TaskCreated;
use App\Events\TaskDeleted;
use App\Events\TaskUpdated;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\TooManyTasksException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    protected $whiteboard;

    public function setUp() : void
    {
        parent::setUp();

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        Model::setEventDispatcher($initialDispatcher);

        $this->whiteboard = factory(Whiteboard::class)->create();
    }

    /** @test */
    public function a_task_broadcasts_task_created_event()
    {
        $task = $this->whiteboard->createTask();

        Event::assertDispatched(TaskCreated::class, null, 'The task didn\'t dispatch a TaskCreated event.');
    }

    /** @test */
    public function a_task_broadcasts_task_updated_event()
    {
        $task = $this->whiteboard->createTask();

        $this->whiteboard->updateTask($task, ['value' => 'Acme']);

        Event::assertDispatched(TaskUpdated::class, null, 'The task didn\'t dispatch a TaskUpdated event.');
    }

    /** @test */
    public function a_task_broadcasts_task_deleted_event()
    {
        $task = $this->whiteboard->createTask();

        $this->whiteboard->deleteTask($task);

        Event::assertDispatched(TaskDeleted::class, null, 'The task didn\'t dispatch a TaskDeleted event.');
    }

    /** @test */
    public function a_task_has_order()
    {
        $task = $this->whiteboard->createTask();

        $this->assertTrue(isset($task->toArray()['order']));
    }
}