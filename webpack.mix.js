const mix = require('laravel-mix');

require('laravel-mix-eslint-config');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/app.js', 'public/js')
   .disableSuccessNotifications()
   .sass('resources/sass/app.scss', 'public/css')
   .webpackConfig({
      resolve: {
         alias: {
            '@': path.resolve('resources/sass'),
         },
      },
   })
   .extract();

if (mix.inProduction()) {
   mix.version();
} else {
   mix.eslint();
}