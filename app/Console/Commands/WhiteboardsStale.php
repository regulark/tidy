<?php

namespace App\Console\Commands;

use App\Whiteboard;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WhiteboardsStale extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'whiteboards:stale';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes stale whiteboards.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Deleting all stale whiteboards...');

        $whiteboards = Whiteboard::allStale();
        $count = $whiteboards->count();

        if ( $count ) {
            Whiteboard::destroy($whiteboards->pluck('id'));

            $this->info("Deleted $count stale whiteboards.");
            Log::info("Deleted $count stale whiteboards.");
        } else {
            $this->info("Nothing stale found.");
        }
    }
}
