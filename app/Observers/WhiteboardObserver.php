<?php

namespace App\Observers;

use App\Whiteboard;
use App\Events\WhiteboardUpdated;
use Facades\App\Dictionary\Dictionary;

class WhiteboardObserver
{
    /**
     * Handle the whiteboard "creating" event.
     *
     * @param  \App\Whiteboard  $whiteboard
     * @return void
     */
    public function creating(Whiteboard $whiteboard)
    {
        $slug = static::generateWordSlug();
            
        // Detect duplicate slugs.
        while ( Whiteboard::where('slug', $slug)->count() ) {
            $slug = generateWordSlug();
        }

        $whiteboard->slug = $slug;
    }

    /**
     * Handle the whiteboard "updated" event.
     *
     * @param  \App\Whiteboard  $whiteboard
     * @return void
     */
    public function updated(Whiteboard $whiteboard)
    {
        // Only broadcast visible attributes.
        $visible = array_filter($whiteboard->getDirty(), function ($value, $key) use ($whiteboard) {
            return in_array($key, $whiteboard->getVisible());
        }, ARRAY_FILTER_USE_BOTH);

        broadcast(new WhiteboardUpdated($whiteboard, $visible))->toOthers();
    }


    /**
     * Generate a word "adjective-noun" slug.
     * 
     * @return string
     */
    static public function generateWordSlug()
    {
        $adjectives = Dictionary::getAdjectives();
        $nouns = Dictionary::getNouns();

        $adjective = $adjectives[array_rand($adjectives)];
        $noun = $nouns[array_rand($nouns)];

        return "${adjective}-${noun}";
    }

    /**
     * Generate a random slug.
     *
     * @param integer $length
     * @param string $chars
     * @return string
     */
    static public function generateRandomSlug($length = 5, $chars = "23456789abcdefghjkmnpqrstuvwxyz") {
        $slug = "";
        $max = strlen($chars);

        for ($i = 0; $i<$length; $i++) {
            $slug .= $chars[random_int(0, $max - 1)];
        }

        return $slug;
    }
}
