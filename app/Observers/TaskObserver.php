<?php

namespace App\Observers;

use App\Task;
use App\Events\TaskCreated;
use App\Events\TaskDeleted;
use App\Events\TaskUpdated;

class TaskObserver
{
    /**
     * Handle the task "creating" event.
     * 
     * @param  \App\Task  $task
     * @return void
     */
    public function creating(Task $task)
    {
        // Assign a random ID.
        // This way users cannot get guess what task IDs.
        while ( is_null($task->id) ) {
            $id = mt_rand(1, 999999);

            if ( ! $task::find($id) ) {
                $task->id = $id;
            }
        }
    }

    /**
     * Handle the task "created" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function created(Task $task)
    {
        broadcast(new TaskCreated($task))->toOthers();
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function updated(Task $task)
    {
        broadcast(new TaskUpdated($task))->toOthers();
    }

    /**
     * Handle the task "deleting" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function deleting(Task $task)
    {
        broadcast(new TaskDeleted($task))->toOthers();
    }
}
