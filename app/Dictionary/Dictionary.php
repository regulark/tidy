<?php

namespace App\Dictionary;

use Illuminate\Contracts\Cache\Repository as Cache;

/**
 * The Dictionary class.
 * 
 * Handles retrieving words to construct Whiteboard slugs.
 */
class Dictionary
{
    const TYPE_NOUN = 'nouns';
    const TYPE_ADJECTIVES = 'adjectives';

    /**
     * How long to store the cached dictionary.
     *
     * @var int
     */
    protected $remember_seconds = 60 * 60 * 24 * 7;

    protected $cache;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Get an array of nouns.
     *
     * @return array
     */
    public function getNouns()
    {
        return $this->getWords(static::TYPE_NOUN);
    }

    /**
     * Get an array of adjectives.
     *
     * @return array
     */
    public function getAdjectives()
    {
        return $this->getWords(static::TYPE_ADJECTIVES);
    }
        
    /**
     * Get a list of words
     *
     * @param string $type The type of word. 
     * @return void
     */
    protected function getWords($type)
    {
        return $this->cache->remember("dictionary-${type}", $this->remember_seconds, function () use ($type) {
            $content = file_get_contents(__DIR__ . "/${type}.txt");

            return explode(PHP_EOL, $content);
        });
    }
}