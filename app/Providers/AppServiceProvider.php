<?php

namespace App\Providers;

use App\Task;
use App\Whiteboard;
use App\Observers\TaskObserver;
use App\Observers\WhiteboardObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Task::observe(TaskObserver::class);
        Whiteboard::observe(WhiteboardObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
