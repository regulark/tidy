<?php

namespace App\Http\Controllers\Api;

use App\Task;
use App\Whiteboard;
use Illuminate\Http\Request;
use App\Exceptions\TooManyTasksException;
use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Task as TaskResource;

class TaskController extends ApiController
{
    /**
     * Retrieve all tasks for a whiteboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Whiteboard $whiteboard)
    {
        return TaskResource::collection($whiteboard->tasks()->get());
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Whiteboard $whiteboard)
    {
        try {
            $task = $whiteboard->createTask($request->all());

            return new TaskResource($task);
        } catch (TooManyTasksException $ex) {
            // Return 422 Unprocessable Entity
            return response('', 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Whiteboard $whiteboard)
    {
        $attributes = $request->validate([
            'id' => 'integer|exists:tasks,id',
            'value' => 'string|max:100|nullable',
            'completed' => 'boolean|nullable',
        ]);

        if ( $request->has('value') && empty($attributes['value']) ) {
            // The user emptied the value, they really want to destroy
            return $this->destroy($request, $whiteboard);
        }

        if ( isset( $attributes['completed'] ) ) {
            $attributes['completed'] = $attributes['completed'] ? true : false;
        }
        
        $task = $whiteboard->updateTask($attributes['id'], $attributes);

        return new TaskResource($task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Whiteboard $whiteboard)
    {
        $attributes = $request->all();

        $whiteboard->deleteTask($attributes['id']);

        return response('destroyed', 200);
    }
}
