<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Whiteboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Whiteboard as WhiteboardResource;

class WhiteboardController extends ApiController
{
    public function __construct() {
        $this->middleware('throttle:4,1')->only('store');
    }
    
    /**
     * Show the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Whiteboard $whiteboard)
    {
        return new WhiteboardResource($whiteboard);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Whiteboard $whiteboard)
    {
        $attributes = $request->validate([
            'title' => 'string|max:100|nullable',
            'description' => 'string|max:500|nullable',
        ]);

        $whiteboard->update($attributes);

        return new WhiteboardResource($whiteboard);
    }
}
