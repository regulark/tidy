<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Whiteboard;
use Illuminate\Http\Request;

class WhiteboardController extends Controller
{
    public function __construct() {
        $this->middleware('throttle:4,1')->only('store');
    }
    
    /**
     * Visit the last whiteboard the user visited, or create a new whiteboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $whiteboard = $this->getHistory($request);

        if ( $whiteboard ) {
            return redirect(route('whiteboard.edit', ['whiteboard' => $whiteboard]));
        }

        return $this->create($request);
    }

    /**
     * Create a new whiteboard and redirect the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return redirect(route('whiteboard.edit', Whiteboard::create()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Whiteboard $whiteboard)
    {
        $this->updateHistory($request, $whiteboard);

        $whiteboard->viewed();

        return view('index', ['whiteboard' => $whiteboard, 'noindex' => true]);
    }

    /**
     * Add a Whiteboard to session history.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whiteboard  $whiteboard
     */
    protected function updateHistory(Request $request, Whiteboard $whiteboard)
    {
        $request->session()->put('previous_whiteboard', $whiteboard->slug);
    }

    /**
     * Retrieve the last visited whiteboard from the session history.
     *
     * @param Request $request
     * @return \App\Whiteboard|null
     */
    protected function getHistory(Request $request)
    {
        $slug = $request->session()->get('previous_whiteboard');

        return Whiteboard::where('slug', $slug)->first();
    }
}
