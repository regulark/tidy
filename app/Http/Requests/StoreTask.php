<?php

namespace App\Http\Requests;

use App\Whiteboard;
use App\Http\Requests\ApiFormRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreTask extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'string|max:100',
            'completed' => 'boolean',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function($validator) {
            if (! $this->whiteboard->canCreateTask() ) {
                $validator->errors()->add('message', 'The limit of tasks has been reached for this whiteboard.');
            }
        });
    }
}
