<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiFormRequest;

class UpdateTask extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'value' => 'nullable|string|max:100',
            'completed' => 'nullable|boolean',
            'order' => 'nullable',
        ];
    }
}
