<?php

namespace App;

use App\Scopes\OrderScope;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $attributes = [
        'value' => '',
        'completed' => false,
        'order' => 0,
    ];

    protected $casts = [
        'completed' => 'boolean',
    ];

    protected $fillable = [
        'value',
        'completed',
        'order',
    ];

    protected $visible = [
        'id',
        'value',
        'completed',
        'order',
    ];

    /**
     * Do not auto-increment the ID.
     * 
     * \App\Observers\TaskObserver handles assinging a random ID to tasks.
     */
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderScope);
    }

    public function whiteboard() {
        return $this->belongsTo('App\Whiteboard');
    }
}
