<?php

namespace App;

use App\Task;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\TooManyTasksException;

class Whiteboard extends Model
{
    protected $fillable = [
        'title',
        'description'
    ];

    protected $visible = [
        'title',
        'description',
        'tasks',
        'tasks-limit'
    ];

    protected $appends = [
        'tasks',
        'channel-name',
        'tasks-limit'
    ];

    public function tasks() {
        return $this->hasMany('App\Task');
    }
    
    public function canCreateTask()
    {
        return ( $this->tasks()->count() < $this->tasks_limit );
    }

    public function createTask($attributes = [])
    {
        $this->guardAgainstTooManyTasks();

        $attributes['order'] = $this->newOrder();

        $task = new Task;
        $task->fill($attributes);

        $this->tasks()->save($task);

        $task->save();
        $task->refresh();

        return $task;
    }

    public function deleteTask($id)
    {
        $task = $this->getTaskFromId($id);

        $task->delete();
    }

    public function updateTask($id, array $attributes) 
    {
        $task = $this->getTaskFromId($id);

        $task->update($attributes);

        return $task;
    }

    public function viewed()
    {
        $this->viewed_at = now();
        $this->save();
    }

    /**
     * Is this whiteboard old and should be removed?
     */
    public function stale()
    {
        if ( $this->isEmpty() ) {
            $interval = CarbonInterval::hours(1);
        } else {
            $interval = CarbonInterval::weeks(1);
        }
        $cutoff = Carbon::now()->sub($interval);

        return $this->viewed_at < $cutoff;
    }

    /**
     * Determine if the whiteboard has never been touched.
     * 
     * @return bool
     */
    public function isEmpty()
    {
        if ( $this->tasks()->count() > 0 ) {
            return false;
        }

        if ( $this->description ) {
            return false;
        }

        if ( $this->title ) {
            return false;
        }

        return true;
    }
    
    public function newOrder()
    {
        $first = $this->tasks()->orderBy('order', 'desc')->first();

        return max($first ? $first->order + 1 : 0, $this->tasks()->count() + 1);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the title, or the slug.
     */
    public function getDisplayTitleAttribute()
    {
        return $this->title ? $this->title : $this->slug;
    }

    /**
     * Get the page's title for this whiteboard.
     *
     * @return string
     */
    public function getPageTitleAttribute() {
        return Str::limit($this->display_title, 16, '...');
    }

    public function getTasksAttribute() {
        return $this->tasks()->get()->toArray();
    }

    public function getChannelNameAttribute() {
        return 'whiteboard.' . $this->slug;
    }

    public function getTasksLimitAttribute() {
        return 50;
    }

    protected function getTaskFromId($id) {
        if ( $id instanceof Task) {
            return $id;
        }

        return $this->tasks()->findOrFail($id);
    }

    protected function guardAgainstTooManyTasks() {
        if (! $this->canCreateTask() ) {
            throw new TooManyTasksException;
        }
    }

    /**
     * Return all `stale` Whiteboards.
     */
    static public function allStale() {
        return Whiteboard::all()->filter->stale();
    }
}