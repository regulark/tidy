<?php

use Faker\Generator as Faker;

$factory->define(App\Whiteboard::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
    ];
});

$factory->defineAs(App\Whiteboard::class, 'empty', function (Faker $faker) {
    return [
        'title' => '',
        'description' => '',
    ];
});
