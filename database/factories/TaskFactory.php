<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'value' => $faker->sentence(),
        'completed' => $faker->boolean(),
    ];
});
