<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/v2/whiteboards/{whiteboard}', 'Api\WhiteboardController@show')->name('whiteboard.show');
Route::patch('/v2/whiteboards/{whiteboard}', 'Api\WhiteboardController@update')->name('whiteboard.update');

Route::get('/v2/whiteboards/{whiteboard}/tasks', 'Api\TaskController@index')->name('task.store');
Route::post('/v2/whiteboards/{whiteboard}/tasks', 'Api\TaskController@store')->name('task.store');
Route::patch('/v2/whiteboards/{whiteboard}/tasks', 'Api\TaskController@update')->name('task.update');
Route::delete('/v2/whiteboards/{whiteboard}/tasks', 'Api\TaskController@destroy')->name('task.destroy');