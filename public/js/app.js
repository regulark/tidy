(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ "./node_modules/redux-thunk/es/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _containers_App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./containers/App */ "./resources/js/containers/App.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _redux_reducers_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./redux/reducers/index */ "./resources/js/redux/reducers/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */









var rootElement = document.getElementById('app');
var store = Object(redux__WEBPACK_IMPORTED_MODULE_6__["createStore"])(_redux_reducers_index__WEBPACK_IMPORTED_MODULE_5__["default"], Object(redux__WEBPACK_IMPORTED_MODULE_6__["applyMiddleware"])(redux_thunk__WEBPACK_IMPORTED_MODULE_1__["default"]));
react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_4__["Provider"], {
  store: store
}, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_App__WEBPACK_IMPORTED_MODULE_3__["default"], {
  data: Object.assign({}, rootElement.dataset)
})), rootElement);

/***/ }),

/***/ "./resources/js/bootstrap.js":
/*!***********************************!*\
  !*** ./resources/js/bootstrap.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var laravel_echo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! laravel-echo */ "./node_modules/laravel-echo/dist/echo.js");
window._ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */


window.Pusher = __webpack_require__(/*! pusher-js */ "./node_modules/pusher-js/dist/web/pusher.js");
window.Echo = new laravel_echo__WEBPACK_IMPORTED_MODULE_0__["default"]({
  broadcaster: 'pusher',
  key: "13b02c4b706b41343fcd",
  cluster: "us2",
  encrypted: true
});

/***/ }),

/***/ "./resources/js/components/AboutDialog.js":
/*!************************************************!*\
  !*** ./resources/js/components/AboutDialog.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core_Dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Dialog */ "./node_modules/@material-ui/core/esm/Dialog/index.js");
/* harmony import */ var _material_ui_core_DialogActions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/DialogActions */ "./node_modules/@material-ui/core/esm/DialogActions/index.js");
/* harmony import */ var _material_ui_core_DialogContent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/DialogContent */ "./node_modules/@material-ui/core/esm/DialogContent/index.js");
/* harmony import */ var _material_ui_core_DialogContentText__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/DialogContentText */ "./node_modules/@material-ui/core/esm/DialogContentText/index.js");
/* harmony import */ var _material_ui_core_DialogTitle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/DialogTitle */ "./node_modules/@material-ui/core/esm/DialogTitle/index.js");









var AboutDialog = function AboutDialog(_ref) {
  var onClose = _ref.onClose,
      open = _ref.open;

  var handleClose = function handleClose() {
    onClose();
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Dialog__WEBPACK_IMPORTED_MODULE_3__["default"], {
    open: open,
    onClose: handleClose,
    "aria-labelledby": "about-dialog-title",
    "aria-describedby": "about-dialog-description"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_DialogTitle__WEBPACK_IMPORTED_MODULE_7__["default"], {
    id: "about-dialog-title"
  }, "About Tidy"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_DialogContent__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_DialogContentText__WEBPACK_IMPORTED_MODULE_6__["default"], {
    id: "about-dialog-description"
  }, "With Tidy, getting your friend and family organized is easy. Just pop onto Tidy, start typing what needs to be done, and share the link with your friends! Done.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_DialogActions__WEBPACK_IMPORTED_MODULE_4__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__["default"], {
    onClick: handleClose,
    color: "primary",
    autoFocus: true
  }, "Okay")));
};

AboutDialog.propTypes = {
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  open: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (AboutDialog);

/***/ }),

/***/ "./resources/js/components/AppFooter.js":
/*!**********************************************!*\
  !*** ./resources/js/components/AppFooter.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Link */ "./node_modules/@material-ui/core/esm/Link/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");





var AppFooter = function AppFooter() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_3__["default"], {
    component: "div"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"], {
    textAlign: "center",
    m: 3,
    color: "text.secondary"
  }, "made by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
    href: "https://seankarol.com"
  }, "sean karol"), " with love"));
};

/* harmony default export */ __webpack_exports__["default"] = (AppFooter);

/***/ }),

/***/ "./resources/js/components/AppHeader.js":
/*!**********************************************!*\
  !*** ./resources/js/components/AppHeader.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _AboutDialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AboutDialog */ "./resources/js/components/AboutDialog.js");
/* harmony import */ var _material_ui_core_AppBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/AppBar */ "./node_modules/@material-ui/core/esm/AppBar/index.js");
/* harmony import */ var _material_ui_core_Toolbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Toolbar */ "./node_modules/@material-ui/core/esm/Toolbar/index.js");
/* harmony import */ var _material_ui_icons_Help__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/Help */ "./node_modules/@material-ui/icons/Help.js");
/* harmony import */ var _material_ui_icons_Help__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Help__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_Create__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/Create */ "./node_modules/@material-ui/icons/Create.js");
/* harmony import */ var _material_ui_icons_Create__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Create__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/IconButton */ "./node_modules/@material-ui/core/esm/IconButton/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_8__["makeStyles"])(function () {
  return {
    root: {
      flexGrow: 1
    },
    title: {
      flexGrow: 1
    }
  };
});

var AppHeader = function AppHeader() {
  var classes = useStyles();

  var _React$useState = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      aboutOpen = _React$useState2[0],
      setAboutOpen = _React$useState2[1];

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: classes.root
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_AppBar__WEBPACK_IMPORTED_MODULE_2__["default"], {
    position: "static"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Toolbar__WEBPACK_IMPORTED_MODULE_3__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_6__["default"], {
    variant: "h6",
    className: classes.title
  }, "Tidy"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_7__["default"], {
    title: "Create new whiteboard",
    "aria-label": "Create new whiteboard",
    "aria-haspopup": "true",
    color: "inherit",
    href: "/new"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Create__WEBPACK_IMPORTED_MODULE_5___default.a, null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_7__["default"], {
    label: "About Tidy",
    "aria-label": "About Tidy",
    "aria-haspopup": "true",
    color: "inherit",
    onClick: function onClick() {
      return setAboutOpen(true);
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Help__WEBPACK_IMPORTED_MODULE_4___default.a, null)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AboutDialog__WEBPACK_IMPORTED_MODULE_1__["default"], {
    open: aboutOpen,
    onClose: function onClose() {
      return setAboutOpen(false);
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (AppHeader);

/***/ }),

/***/ "./resources/js/components/DebounceTextArea.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/DebounceTextArea.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var DebounceTextArea =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DebounceTextArea, _React$Component);

  function DebounceTextArea(props) {
    var _this;

    _classCallCheck(this, DebounceTextArea);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DebounceTextArea).call(this, props));
    _this.state = {
      value: _this.props.value || ''
    };
    _this.changed = lodash_debounce__WEBPACK_IMPORTED_MODULE_2___default()(_this.props.onChange, 250);
    _this.ref = react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef();
    return _this;
  }

  _createClass(DebounceTextArea, [{
    key: "handleChange",
    value: function handleChange(event) {
      var _this2 = this;

      var value = this.props.preChange ? this.props.preChange(event) : event.target.value;
      this.setState({
        value: value
      }, function () {
        _this2.changed(value);
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      // If the prop value has changed, then update the state value to match.
      if (this.props.value !== prevProps.value) {
        this.setState({
          value: this.props.value
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      // eslint-disable-next-line no-unused-vars
      var _this$props = this.props,
          preChange = _this$props.preChange,
          onChange = _this$props.onChange,
          props = _objectWithoutProperties(_this$props, ["preChange", "onChange"]);

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, props, {
        type: "text",
        value: this.state.value,
        ref: this.ref,
        fullWidth: true,
        multiline: true,
        onChange: function onChange() {
          return null;
        },
        onInput: function onInput() {
          return _this3.handleChange(event);
        }
      }));
    }
  }]);

  return DebounceTextArea;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

DebounceTextArea.propTypes = {
  value: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  preChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (DebounceTextArea);

/***/ }),

/***/ "./resources/js/components/TaskCompleted.js":
/*!**************************************************!*\
  !*** ./resources/js/components/TaskCompleted.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Checkbox */ "./node_modules/@material-ui/core/esm/Checkbox/index.js");




var TaskCompleted = function TaskCompleted(_ref) {
  var task = _ref.task,
      onToggle = _ref.onToggle;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_2__["default"], {
    edge: "start",
    type: "checkbox",
    checked: task.completed,
    onChange: function onChange(event) {
      return onToggle(event.target.checked);
    }
  });
};

TaskCompleted.propTypes = {
  task: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  onToggle: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (TaskCompleted);

/***/ }),

/***/ "./resources/js/components/TaskCreate.js":
/*!***********************************************!*\
  !*** ./resources/js/components/TaskCreate.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/IconButton */ "./node_modules/@material-ui/core/esm/IconButton/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_icons_AddCircle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/AddCircle */ "./node_modules/@material-ui/icons/AddCircle.js");
/* harmony import */ var _material_ui_icons_AddCircle__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AddCircle__WEBPACK_IMPORTED_MODULE_6__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__["makeStyles"])(function () {
  return {
    container: {
      display: 'flex',
      alignItems: 'center'
    },
    textField: {},
    submitButton: {}
  };
});

var TaskCreate = function TaskCreate(_ref) {
  var disabled = _ref.disabled,
      createTask = _ref.createTask;
  var classes = useStyles();

  var _React$useState = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(''),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      value = _React$useState2[0],
      setValue = _React$useState2[1];

  var trimReturns = function trimReturns(value) {
    return value.replace('\n', '');
  };

  var handleChange = function handleChange(event) {
    setValue(trimReturns(event.target.value));
  };

  var handleKeyDown = function handleKeyDown(event) {
    if (13 === event.keyCode) {
      handleSubmit(event);
    }
  };

  var handleSubmit = function handleSubmit(event) {
    if (!disabled && value) {
      createTask({
        value: value
      });
      setValue('');
    }

    event.preventDefault();
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    onSubmit: function onSubmit(event) {
      return handleSubmit(event);
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    display: "block",
    p: 1,
    m: 1,
    className: classes.container
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__["default"], {
    disabled: disabled,
    inputProps: {
      maxLength: 100
    },
    placeholder: "Add a new task",
    className: classes.textField,
    multiline: true,
    fullWidth: true,
    autoFocus: true,
    value: value || '',
    onKeyDown: function onKeyDown(event) {
      return handleKeyDown(event);
    },
    onChange: function onChange(event) {
      return handleChange(event);
    }
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_4__["default"], {
    type: "submit",
    disabled: disabled || !value,
    "aria-label": "submit",
    className: classes.submitButton,
    color: "secondary",
    edge: "end"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_AddCircle__WEBPACK_IMPORTED_MODULE_6___default.a, null))));
};

TaskCreate.propTypes = {
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired,
  createTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (TaskCreate);

/***/ }),

/***/ "./resources/js/components/TaskDelete.js":
/*!***********************************************!*\
  !*** ./resources/js/components/TaskDelete.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/icons/Delete */ "./node_modules/@material-ui/icons/Delete.js");
/* harmony import */ var _material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/IconButton */ "./node_modules/@material-ui/core/esm/IconButton/index.js");





var TaskDelete = function TaskDelete(_ref) {
  var onDelete = _ref.onDelete;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
    edge: "end",
    "aria-label": "delete",
    onClick: function onClick() {
      return onDelete();
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_2___default.a, null));
};

TaskDelete.propTypes = {
  task: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  onDelete: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (TaskDelete);

/***/ }),

/***/ "./resources/js/components/TaskItem.js":
/*!*********************************************!*\
  !*** ./resources/js/components/TaskItem.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TaskDelete__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TaskDelete */ "./resources/js/components/TaskDelete.js");
/* harmony import */ var _TaskValue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TaskValue */ "./resources/js/components/TaskValue.js");
/* harmony import */ var _TaskCompleted__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./TaskCompleted */ "./resources/js/components/TaskCompleted.js");
/* harmony import */ var _material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/ListItem */ "./node_modules/@material-ui/core/esm/ListItem/index.js");







var TaskItem = function TaskItem(_ref) {
  var task = _ref.task,
      onChange = _ref.onChange,
      onToggle = _ref.onToggle,
      onDelete = _ref.onDelete;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TaskCompleted__WEBPACK_IMPORTED_MODULE_4__["default"], {
    task: task,
    onToggle: onToggle
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TaskValue__WEBPACK_IMPORTED_MODULE_3__["default"], {
    task: task,
    onChange: onChange
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TaskDelete__WEBPACK_IMPORTED_MODULE_2__["default"], {
    task: task,
    onDelete: onDelete
  }));
};

TaskItem.propTypes = {
  task: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape({
    id: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
    value: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
    completed: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired
  }).isRequired,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  onToggle: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  onDelete: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (TaskItem);

/***/ }),

/***/ "./resources/js/components/TaskList.js":
/*!*********************************************!*\
  !*** ./resources/js/components/TaskList.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TaskItem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TaskItem */ "./resources/js/components/TaskItem.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_List__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/List */ "./node_modules/@material-ui/core/esm/List/index.js");
/* harmony import */ var _material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/lab/Skeleton */ "./node_modules/@material-ui/lab/esm/Skeleton/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
/* harmony import */ var _redux_selectors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../redux/selectors */ "./resources/js/redux/selectors.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }










var TaskList = function TaskList(_ref) {
  var tasks = _ref.tasks,
      updateTask = _ref.updateTask,
      deleteTask = _ref.deleteTask;

  var listItems = function listItems(tasks) {
    return tasks.map(function (task) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TaskItem__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
        key: task.id,
        task: task
      }, task, {
        onToggle: function onToggle(completed) {
          return updateTask(task.id, {
            completed: completed
          });
        },
        onChange: function onChange(value) {
          return updateTask(task.id, {
            value: value
          });
        },
        onDelete: function onDelete() {
          return deleteTask(task.id);
        }
      }));
    });
  };

  var emptyItems = function emptyItems(items) {
    var text = "There are no tasks here.";

    if (items.length === 0) {
      text = "Start by adding tasks below.";
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_6__["default"], {
      component: "div"
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
      color: "text.secondary",
      py: 2.5,
      mx: 2
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, text)));
  };

  var skeletonItems = function skeletonItems() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
      color: "text.secondary",
      py: 2,
      mx: 2
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_5__["default"], {
      variant: "rect",
      height: 36
    }));
  };

  var visibleItems = Object(_redux_selectors__WEBPACK_IMPORTED_MODULE_7__["getTasksByVisibilityFilter"])(tasks.items, tasks.visibilityFilter);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_List__WEBPACK_IMPORTED_MODULE_4__["default"], null, tasks.loaded ? visibleItems.length ? listItems(visibleItems) : emptyItems(tasks.items) : skeletonItems());
};

TaskList.propTypes = {
  tasks: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired,
  updateTask: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func.isRequired,
  deleteTask: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (TaskList);

/***/ }),

/***/ "./resources/js/components/TaskValue.js":
/*!**********************************************!*\
  !*** ./resources/js/components/TaskValue.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _DebounceTextArea__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DebounceTextArea */ "./resources/js/components/DebounceTextArea.js");




var TaskValue = function TaskValue(_ref) {
  var task = _ref.task,
      _onChange = _ref.onChange;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DebounceTextArea__WEBPACK_IMPORTED_MODULE_2__["default"], {
    placeholder: "Task",
    maxLength: "100",
    value: task.value || '',
    preChange: function preChange(event) {
      return event.target.value.replace('\n', '');
    },
    onChange: function onChange(title) {
      return _onChange(title);
    }
  });
};

TaskValue.propTypes = {
  task: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape({
    id: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
    value: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
    completed: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired
  }).isRequired,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (TaskValue);

/***/ }),

/***/ "./resources/js/components/VisibilityFilters.js":
/*!******************************************************!*\
  !*** ./resources/js/components/VisibilityFilters.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Tab */ "./node_modules/@material-ui/core/esm/Tab/index.js");
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Tabs */ "./node_modules/@material-ui/core/esm/Tabs/index.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../constants */ "./resources/js/constants.js");
/* harmony import */ var _redux_actions_tasks__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../redux/actions/tasks */ "./resources/js/redux/actions/tasks.js");
/* harmony import */ var _redux_selectors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../redux/selectors */ "./resources/js/redux/selectors.js");









var VisibilityFilters = function VisibilityFilters(_ref) {
  var tasks = _ref.tasks,
      activeFilter = _ref.activeFilter,
      setTasksVisibilityFilter = _ref.setTasksVisibilityFilter;

  var handleChange = function handleChange(event, newValue) {
    setTasksVisibilityFilter(_constants__WEBPACK_IMPORTED_MODULE_5__["VISIBILITY_FILTERS"][newValue]);
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_4__["default"], {
    value: activeFilter,
    onChange: handleChange,
    "aria-label": "Set task visibility",
    variant: "fullWidth"
  }, Object.keys(_constants__WEBPACK_IMPORTED_MODULE_5__["VISIBILITY_FILTERS"]).map(function (filterKey) {
    var currentFilter = _constants__WEBPACK_IMPORTED_MODULE_5__["VISIBILITY_FILTERS"][filterKey];
    var amount = Object(_redux_selectors__WEBPACK_IMPORTED_MODULE_7__["getTasksByVisibilityFilter"])(tasks, currentFilter).length;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_3__["default"], {
      label: "".concat(currentFilter, " (").concat(amount, ")"),
      value: filterKey,
      key: currentFilter
    });
  }));
};

VisibilityFilters.propTypes = {
  tasks: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array.isRequired,
  activeFilter: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  setTasksVisibilityFilter: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    tasks: state.tasks.items,
    activeFilter: state.tasks.visibilityFilter
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, {
  setTasksVisibilityFilter: _redux_actions_tasks__WEBPACK_IMPORTED_MODULE_6__["setTasksVisibilityFilter"]
})(VisibilityFilters));

/***/ }),

/***/ "./resources/js/components/Whiteboard.js":
/*!***********************************************!*\
  !*** ./resources/js/components/Whiteboard.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TaskList__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TaskList */ "./resources/js/components/TaskList.js");
/* harmony import */ var _TaskCreate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TaskCreate */ "./resources/js/components/TaskCreate.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Paper */ "./node_modules/@material-ui/core/esm/Paper/index.js");
/* harmony import */ var _WhiteboardMeta__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./WhiteboardMeta */ "./resources/js/components/WhiteboardMeta.js");
/* harmony import */ var _WhiteboardTitle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./WhiteboardTitle */ "./resources/js/components/WhiteboardTitle.js");
/* harmony import */ var _VisibilityFilters__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./VisibilityFilters */ "./resources/js/components/VisibilityFilters.js");
/* harmony import */ var _WhiteboardDescription__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./WhiteboardDescription */ "./resources/js/components/WhiteboardDescription.js");











var Whiteboard = function Whiteboard(_ref) {
  var whiteboard = _ref.whiteboard,
      updateWhiteboard = _ref.updateWhiteboard,
      tasks = _ref.tasks,
      createTask = _ref.createTask,
      updateTask = _ref.updateTask,
      deleteTask = _ref.deleteTask;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_4__["default"], {
    mb: 3
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_WhiteboardTitle__WEBPACK_IMPORTED_MODULE_7__["default"], {
    whiteboard: whiteboard,
    updateWhiteboard: updateWhiteboard
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_WhiteboardDescription__WEBPACK_IMPORTED_MODULE_9__["default"], {
    whiteboard: whiteboard,
    updateWhiteboard: updateWhiteboard
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_WhiteboardMeta__WEBPACK_IMPORTED_MODULE_6__["default"], {
    whiteboard: whiteboard
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_4__["default"], {
    mb: 3
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_VisibilityFilters__WEBPACK_IMPORTED_MODULE_8__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TaskList__WEBPACK_IMPORTED_MODULE_2__["default"], {
    tasks: tasks,
    updateTask: updateTask,
    deleteTask: deleteTask
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TaskCreate__WEBPACK_IMPORTED_MODULE_3__["default"], {
    disabled: !tasks.loaded,
    createTask: createTask
  })));
};

Whiteboard.propTypes = {
  whiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  updateWhiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  tasks: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  createTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  updateTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  deleteTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Whiteboard);

/***/ }),

/***/ "./resources/js/components/WhiteboardDescription.js":
/*!**********************************************************!*\
  !*** ./resources/js/components/WhiteboardDescription.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/lab/Skeleton */ "./node_modules/@material-ui/lab/esm/Skeleton/index.js");
/* harmony import */ var _DebounceTextArea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./DebounceTextArea */ "./resources/js/components/DebounceTextArea.js");






var WhiteboardDescription = function WhiteboardDescription(_ref) {
  var whiteboard = _ref.whiteboard,
      updateWhiteboard = _ref.updateWhiteboard;

  var handleChange = function handleChange(description) {
    updateWhiteboard({
      description: description
    });
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    display: "block",
    pb: 2,
    px: 1,
    m: 1
  }, whiteboard.loaded ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DebounceTextArea__WEBPACK_IMPORTED_MODULE_4__["default"], {
    inputProps: {
      maxLength: 500
    },
    label: "Description",
    maxLength: "500",
    rowsMax: "6",
    value: whiteboard.description || '',
    onChange: function onChange(description) {
      return handleChange(description);
    }
  }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_3__["default"], {
    variant: "rect",
    height: 32
  }));
};

WhiteboardDescription.propTypes = {
  whiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  updateWhiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (WhiteboardDescription);

/***/ }),

/***/ "./resources/js/components/WhiteboardMeta.js":
/*!***************************************************!*\
  !*** ./resources/js/components/WhiteboardMeta.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/lab/Skeleton */ "./node_modules/@material-ui/lab/esm/Skeleton/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");







var WhiteboardMeta = function WhiteboardMeta(_ref) {
  var whiteboard = _ref.whiteboard;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
    display: "block",
    pb: 2,
    px: 1,
    m: 1,
    color: "text.secondary"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_5__["default"], {
    variant: "body2"
  }, whiteboard.loaded ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Created ", moment__WEBPACK_IMPORTED_MODULE_1___default()(whiteboard.created_at).fromNow(), ".") : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_4__["default"], {
    component: "span"
  })));
};

WhiteboardMeta.propTypes = {
  whiteboard: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (WhiteboardMeta);

/***/ }),

/***/ "./resources/js/components/WhiteboardTitle.js":
/*!****************************************************!*\
  !*** ./resources/js/components/WhiteboardTitle.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/lab/Skeleton */ "./node_modules/@material-ui/lab/esm/Skeleton/index.js");
/* harmony import */ var _DebounceTextArea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./DebounceTextArea */ "./resources/js/components/DebounceTextArea.js");






var WhiteboardTitle = function WhiteboardTitle(_ref) {
  var whiteboard = _ref.whiteboard,
      updateWhiteboard = _ref.updateWhiteboard;

  var _preChange = function preChange(event) {
    return event.target.value.replace('\n', '');
  };

  var handleChange = function handleChange(title) {
    updateWhiteboard({
      title: title
    });
  };

  var catchReturn = function catchReturn(event) {
    if (13 === event.keyCode) {
      event.preventDefault();
    }
  };

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    display: "block",
    py: 2,
    px: 1,
    m: 1
  }, whiteboard.loaded ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DebounceTextArea__WEBPACK_IMPORTED_MODULE_4__["default"], {
    inputProps: {
      maxLength: 100
    },
    label: "Title",
    value: whiteboard.title || '',
    onKeyDown: function onKeyDown(event) {
      return catchReturn(event);
    },
    preChange: function preChange(event) {
      return _preChange(event);
    },
    onChange: function onChange(title) {
      return handleChange(title);
    }
  }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_3__["default"], {
    variant: "rect",
    height: 32
  }));
};

WhiteboardTitle.propTypes = {
  whiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  updateWhiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (WhiteboardTitle);

/***/ }),

/***/ "./resources/js/constants.js":
/*!***********************************!*\
  !*** ./resources/js/constants.js ***!
  \***********************************/
/*! exports provided: VISIBILITY_FILTERS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VISIBILITY_FILTERS", function() { return VISIBILITY_FILTERS; });
var VISIBILITY_FILTERS = {
  ALL: "ALL",
  COMPLETED: "COMPLETED",
  INCOMPLETE: "INCOMPLETE"
};

/***/ }),

/***/ "./resources/js/containers/App.js":
/*!****************************************!*\
  !*** ./resources/js/containers/App.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _pages_PageError__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/PageError */ "./resources/js/containers/pages/PageError.js");
/* harmony import */ var _components_AppHeader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/AppHeader */ "./resources/js/components/AppHeader.js");
/* harmony import */ var _components_AppFooter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/AppFooter */ "./resources/js/components/AppFooter.js");
/* harmony import */ var _pages_PageWhiteboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/PageWhiteboard */ "./resources/js/containers/pages/PageWhiteboard.js");
/* harmony import */ var _material_ui_core_Container__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/Container */ "./node_modules/@material-ui/core/esm/Container/index.js");
/* harmony import */ var _material_ui_core_colors_purple__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/colors/purple */ "./node_modules/@material-ui/core/colors/purple.js");
/* harmony import */ var _material_ui_core_colors_purple__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_colors_purple__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_core_CssBaseline__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/CssBaseline */ "./node_modules/@material-ui/core/esm/CssBaseline/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");












var App = function App(_ref) {
  var data = _ref.data;
  var outerTheme = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__["createMuiTheme"])({
    palette: {
      primary: _material_ui_core_colors_purple__WEBPACK_IMPORTED_MODULE_8___default.a,
      secondary: {
        main: '#f44336'
      }
    }
  });
  var slug = window.location.pathname.match(/([\w-]+)/)[0];
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__["ThemeProvider"], {
    theme: outerTheme
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_CssBaseline__WEBPACK_IMPORTED_MODULE_9__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    mb: 3
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_AppHeader__WEBPACK_IMPORTED_MODULE_4__["default"], null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Container__WEBPACK_IMPORTED_MODULE_7__["default"], {
    fixed: true
  }, typeof data.statusCode === 'string' ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_PageError__WEBPACK_IMPORTED_MODULE_3__["default"], {
    statusCode: data.statusCode
  }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_PageWhiteboard__WEBPACK_IMPORTED_MODULE_6__["default"], {
    slug: slug
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_AppFooter__WEBPACK_IMPORTED_MODULE_5__["default"], null));
};

App.propTypes = {
  data: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object
};
/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./resources/js/containers/pages/PageError.js":
/*!****************************************************!*\
  !*** ./resources/js/containers/pages/PageError.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }






var PageError = function PageError(_ref) {
  var statusCode = _ref.statusCode;
  var codes = {
    '404': {
      'title': 'Page Not Found',
      'body': 'If you were looking for a whiteboard, it probably expired.'
    }
  };
  var code = _typeof(codes[parseInt(statusCode)]) === 'object' ? codes[parseInt(statusCode)] : {
    'title': 'Uknown Error',
    'body': 'Something unexpected has happened!'
  };
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_3__["default"], {
    align: "center",
    component: "div"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_3__["default"], {
    component: "div"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, code['title']), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, code['body']), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    color: "text.secondary"
  }, "Code ", statusCode)));
};

PageError.propTypes = {
  statusCode: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (PageError);

/***/ }),

/***/ "./resources/js/containers/pages/PageWhiteboard.js":
/*!*********************************************************!*\
  !*** ./resources/js/containers/pages/PageWhiteboard.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_Whiteboard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/Whiteboard */ "./resources/js/components/Whiteboard.js");
/* harmony import */ var _redux_actions_whiteboard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../redux/actions/whiteboard */ "./resources/js/redux/actions/whiteboard.js");
/* harmony import */ var _redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../redux/actions/tasks */ "./resources/js/redux/actions/tasks.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/* globals Echo */







var PageWhiteboard =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PageWhiteboard, _React$Component);

  function PageWhiteboard() {
    _classCallCheck(this, PageWhiteboard);

    return _possibleConstructorReturn(this, _getPrototypeOf(PageWhiteboard).apply(this, arguments));
  }

  _createClass(PageWhiteboard, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this = this;

      var slug = this.props.slug;
      this.props.fetchWhiteboard(slug);
      this.props.fetchTasks(slug);
      Echo.channel("whiteboard.".concat(slug)).listen('WhiteboardUpdated', function (e) {
        _this.props.updateWhiteboard(e.data);
      }).listen('TaskCreated', function (e) {
        _this.props.createTask(e.task);
      }).listen('TaskUpdated', function (e) {
        _this.props.updateTask(e.data.id, e.data);
      }).listen('TaskDeleted', function (e) {
        _this.props.deleteTask(e.task.id);
      });
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Whiteboard__WEBPACK_IMPORTED_MODULE_3__["default"], {
        whiteboard: this.props.whiteboard,
        tasks: this.props.tasks,
        updateWhiteboard: this.props.userUpdateWhiteboard,
        createTask: this.props.userCreateTask,
        updateTask: this.props.userUpdateTask,
        deleteTask: this.props.userDeleteTask
      }));
    }
  }]);

  return PageWhiteboard;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

PageWhiteboard.propTypes = {
  slug: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  whiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  tasks: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  fetchWhiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  updateWhiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  fetchTasks: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  createTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  updateTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  deleteTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  userUpdateWhiteboard: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  userCreateTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  userUpdateTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  userDeleteTask: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    whiteboard: state.whiteboard,
    tasks: state.tasks
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
  return {
    fetchWhiteboard: function fetchWhiteboard(slug) {
      return dispatch(Object(_redux_actions_whiteboard__WEBPACK_IMPORTED_MODULE_4__["fetchWhiteboard"])(slug));
    },
    updateWhiteboard: function updateWhiteboard(changes) {
      return dispatch(Object(_redux_actions_whiteboard__WEBPACK_IMPORTED_MODULE_4__["updateWhiteboard"])(changes));
    },
    fetchTasks: function fetchTasks(slug) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["fetchTasks"])(slug));
    },
    createTask: function createTask(attributes) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["createTask"])(attributes));
    },
    updateTask: function updateTask(id, changes) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["updateTask"])(id, changes));
    },
    deleteTask: function deleteTask(id) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["deleteTask"])(id));
    },
    userUpdateWhiteboard: function userUpdateWhiteboard(changes) {
      return dispatch(Object(_redux_actions_whiteboard__WEBPACK_IMPORTED_MODULE_4__["userUpdateWhiteboard"])(ownProps.slug, changes));
    },
    userCreateTask: function userCreateTask(attributes) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["userCreateTask"])(ownProps.slug, attributes));
    },
    userUpdateTask: function userUpdateTask(id, changes) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["userUpdateTask"])(ownProps.slug, id, changes));
    },
    userDeleteTask: function userDeleteTask(id) {
      return dispatch(Object(_redux_actions_tasks__WEBPACK_IMPORTED_MODULE_5__["userDeleteTask"])(ownProps.slug, id));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(PageWhiteboard));

/***/ }),

/***/ "./resources/js/redux/actions/actionTypes.js":
/*!***************************************************!*\
  !*** ./resources/js/redux/actions/actionTypes.js ***!
  \***************************************************/
/*! exports provided: UPDATE_WHITEBOARD, CREATE_TASK, UPDATE_TASKS, UPDATE_TASK, DELETE_TASK, SET_TASKS_VISIBILITY */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_WHITEBOARD", function() { return UPDATE_WHITEBOARD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATE_TASK", function() { return CREATE_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_TASKS", function() { return UPDATE_TASKS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_TASK", function() { return UPDATE_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_TASK", function() { return DELETE_TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TASKS_VISIBILITY", function() { return SET_TASKS_VISIBILITY; });
var UPDATE_WHITEBOARD = "UPDATE_WHITEBOARD";
var CREATE_TASK = "CREATE_TASLK";
var UPDATE_TASKS = "UPDATE_TASKS";
var UPDATE_TASK = "UPDATE_TASK";
var DELETE_TASK = "DELETE_TASK";
var SET_TASKS_VISIBILITY = "SET_TASKS_VISIBILITY";

/***/ }),

/***/ "./resources/js/redux/actions/tasks.js":
/*!*********************************************!*\
  !*** ./resources/js/redux/actions/tasks.js ***!
  \*********************************************/
/*! exports provided: endpoint, fetchTasks, createTask, userCreateTask, updateTasks, updateTask, userUpdateTask, userDeleteTask, deleteTask, setTasksVisibilityFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endpoint", function() { return endpoint; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTasks", function() { return fetchTasks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTask", function() { return createTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userCreateTask", function() { return userCreateTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateTasks", function() { return updateTasks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateTask", function() { return updateTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userUpdateTask", function() { return userUpdateTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userDeleteTask", function() { return userDeleteTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteTask", function() { return deleteTask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setTasksVisibilityFilter", function() { return setTasksVisibilityFilter; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actionTypes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actionTypes */ "./resources/js/redux/actions/actionTypes.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var endpoint = function endpoint(slug) {
  return "/api/v2/whiteboards/".concat(slug, "/tasks");
};
var fetchTasks = function fetchTasks(slug) {
  return function (dispatch) {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(endpoint(slug)).then(function (_ref) {
      var data = _ref.data;
      dispatch(updateTasks(data.data));
    });
  };
};
var createTask = function createTask(attributes) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["CREATE_TASK"],
    data: attributes
  };
};
var userCreateTask = function userCreateTask(slug, attributes) {
  return function (dispatch) {
    var data = _objectSpread({}, attributes, {
      _method: 'POST'
    });

    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(endpoint(slug), data).then(function (_ref2) {
      var data = _ref2.data;
      dispatch(createTask(data.data));
    });
  };
};
var updateTasks = function updateTasks(data) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["UPDATE_TASKS"],
    data: data
  };
};
var updateTask = function updateTask(id, changes) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["UPDATE_TASK"],
    data: _objectSpread({
      id: id
    }, changes)
  };
};
var userUpdateTask = function userUpdateTask(slug, id, changes) {
  var data = _objectSpread({
    id: id
  }, changes, {
    _method: 'PATCH'
  });

  axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(endpoint(slug), data).catch(function (err) {
    throw err;
  });
  return updateTask(id, data);
};
var userDeleteTask = function userDeleteTask(slug, id) {
  var data = {
    id: id,
    _method: 'DELETE'
  };
  axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(endpoint(slug), data).catch(function (err) {
    throw err;
  });
  return deleteTask(id);
};
var deleteTask = function deleteTask(id) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["DELETE_TASK"],
    data: {
      id: id
    }
  };
};
var setTasksVisibilityFilter = function setTasksVisibilityFilter(filter) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["SET_TASKS_VISIBILITY"],
    data: {
      filter: filter
    }
  };
};

/***/ }),

/***/ "./resources/js/redux/actions/whiteboard.js":
/*!**************************************************!*\
  !*** ./resources/js/redux/actions/whiteboard.js ***!
  \**************************************************/
/*! exports provided: endpoint, fetchWhiteboard, updateWhiteboard, userUpdateWhiteboard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endpoint", function() { return endpoint; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchWhiteboard", function() { return fetchWhiteboard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateWhiteboard", function() { return updateWhiteboard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userUpdateWhiteboard", function() { return userUpdateWhiteboard; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actionTypes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actionTypes */ "./resources/js/redux/actions/actionTypes.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var endpoint = function endpoint(slug) {
  return "/api/v2/whiteboards/".concat(slug);
};
var fetchWhiteboard = function fetchWhiteboard(slug) {
  return function (dispatch) {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(endpoint(slug)).then(function (_ref) {
      var data = _ref.data;
      dispatch(updateWhiteboard(data.data));
    }).catch(function (error) {
      dispatch(updateWhiteboard({
        failed: true
      }));
      throw error;
    });
  };
};
var updateWhiteboard = function updateWhiteboard(changes) {
  return {
    type: _actionTypes__WEBPACK_IMPORTED_MODULE_1__["UPDATE_WHITEBOARD"],
    data: _objectSpread({}, changes)
  };
};
var userUpdateWhiteboard = function userUpdateWhiteboard(slug, changes) {
  var data = _objectSpread({}, changes, {
    _method: 'patch'
  });

  axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(endpoint(slug), data).catch(function (err) {
    throw err;
  });
  return updateWhiteboard(changes);
};

/***/ }),

/***/ "./resources/js/redux/reducers/index.js":
/*!**********************************************!*\
  !*** ./resources/js/redux/reducers/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");
/* harmony import */ var _whiteboard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./whiteboard */ "./resources/js/redux/reducers/whiteboard.js");
/* harmony import */ var _tasks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tasks */ "./resources/js/redux/reducers/tasks.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  whiteboard: _whiteboard__WEBPACK_IMPORTED_MODULE_1__["default"],
  tasks: _tasks__WEBPACK_IMPORTED_MODULE_2__["default"]
}));

/***/ }),

/***/ "./resources/js/redux/reducers/tasks.js":
/*!**********************************************!*\
  !*** ./resources/js/redux/reducers/tasks.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../constants */ "./resources/js/constants.js");
/* harmony import */ var _actions_actionTypes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../actions/actionTypes */ "./resources/js/redux/actions/actionTypes.js");
/* harmony import */ var _selectors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../selectors */ "./resources/js/redux/selectors.js");
function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var initialState = {
  loaded: false,
  visibilityFilter: _constants__WEBPACK_IMPORTED_MODULE_0__["VISIBILITY_FILTERS"].INCOMPLETE,
  items: []
};

var tasks = function tasks() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_1__["CREATE_TASK"]:
      {
        if (Object(_selectors__WEBPACK_IMPORTED_MODULE_2__["getTaskById"])(state.items, action.data.id)) {
          return state;
        }

        return _objectSpread({}, state, {
          items: state.items.concat([action.data])
        });
      }

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_1__["UPDATE_TASKS"]:
      {
        return _objectSpread({}, state, {
          loaded: true,
          items: action.data
        });
      }

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_1__["UPDATE_TASK"]:
      {
        var _action$data = action.data,
            id = _action$data.id,
            changes = _objectWithoutProperties(_action$data, ["id"]);

        return _objectSpread({}, state, {
          items: state.items.map(function (task) {
            return task.id === id ? _objectSpread({}, task, changes) : _objectSpread({}, task);
          })
        });
      }

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_1__["DELETE_TASK"]:
      {
        var _id = action.data.id;
        return _objectSpread({}, state, {
          items: state.items.filter(function (task) {
            return task.id !== _id;
          })
        });
      }

    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_1__["SET_TASKS_VISIBILITY"]:
      {
        return _objectSpread({}, state, {
          visibilityFilter: action.data.filter
        });
      }

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (tasks);

/***/ }),

/***/ "./resources/js/redux/reducers/whiteboard.js":
/*!***************************************************!*\
  !*** ./resources/js/redux/reducers/whiteboard.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/actionTypes */ "./resources/js/redux/actions/actionTypes.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var initialState = {
  loaded: false,
  failed: false
};

var whiteboard = function whiteboard() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actions_actionTypes__WEBPACK_IMPORTED_MODULE_0__["UPDATE_WHITEBOARD"]:
      {
        var data = action.data;
        return _objectSpread({}, state, data, {
          loaded: true
        });
      }

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (whiteboard);

/***/ }),

/***/ "./resources/js/redux/selectors.js":
/*!*****************************************!*\
  !*** ./resources/js/redux/selectors.js ***!
  \*****************************************/
/*! exports provided: getTasks, getTaskById, getTasksByVisibilityFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTasks", function() { return getTasks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTaskById", function() { return getTaskById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTasksByVisibilityFilter", function() { return getTasksByVisibilityFilter; });
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants */ "./resources/js/constants.js");

var getTasks = function getTasks(store) {
  return store.tasks.items;
};
var getTaskById = function getTaskById(tasks, id) {
  return tasks.find(function (task) {
    return task.id === id;
  });
};
var getTasksByVisibilityFilter = function getTasksByVisibilityFilter(tasks, visibilityFilter) {
  switch (visibilityFilter) {
    case _constants__WEBPACK_IMPORTED_MODULE_0__["VISIBILITY_FILTERS"].COMPLETED:
      return tasks.filter(function (task) {
        return task.completed;
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["VISIBILITY_FILTERS"].INCOMPLETE:
      return tasks.filter(function (task) {
        return !task.completed;
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["VISIBILITY_FILTERS"].ALL:
    default:
      return tasks;
  }
};

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/sean/sites/tidy/resources/js/app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! /Users/sean/sites/tidy/resources/sass/app.scss */"./resources/sass/app.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);